package com.example.impl;

import javax.jws.WebService;

import com.example.Calc;
//  name="Calculator", portName="prtNm", endpointInterface="com.example.Calc",

@WebService(serviceName="CalculatorWebService",  targetNamespace="http://example.ws.com/", 
	name="Calculator", portName="CalculatorPort")
public class CalcImpl implements Calc {

	@Override
	public int add(int a, int b) {
		return a+b;
	}

}
